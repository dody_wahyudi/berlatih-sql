1. Membuat Database :
create database myshop;

2. Membuat Table di Dalam Database :
- Table user :
MariaDB [myshop]> create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories :
MariaDB [myshop]> create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

Table Users :
MariaDB [myshop]> create table items(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(10),
    -> stock int(10),
    -> category_id int(10),
    -> foreign key(category_id) references categories(id)
    -> );

3. Memasukkan Data Pada Table :
users :
MariaDB [myshop]> insert into users(name, email, password)
    -> values("John Doe", "john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

categories :
MariaDB [myshop]> insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.001 sec)

items :
MariaDB [myshop]> insert into items(name, description, price, stock,category_id)
    -> values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil Data dari Database :
a. Mengambil data users (Kecuali Password)
MariaDB [myshop]> select id, name, email from users;

b. Mengambil data items
MariaDB [myshop]> select * from items where price > 1000000;
MariaDB [myshop]> select * from items where name like 'uniklo%';
MariaDB [myshop]> select * from items where name like '%watch';

c. Menampilkan data items join dengan kategori
MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id,
    -> categories.name as kategori from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database
MariaDB [myshop]> update items set price=2500000 where name = "Sumsang b50";
Query OK, 1 row affected (0.001 sec)

